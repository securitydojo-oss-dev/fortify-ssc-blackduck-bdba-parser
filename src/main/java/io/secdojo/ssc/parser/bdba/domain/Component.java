package io.secdojo.ssc.parser.bdba.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public final class Component {

    @JsonProperty("Component")
    String component;
    @JsonProperty("Component type")
    String componentType;
    @JsonProperty("Distribution package")
    String distributionPackage;
    @JsonProperty("Latest version")
    String latestVersion;
    @JsonProperty("License")
    String license;
    @JsonProperty("License type")
    String licenseType;
    @JsonProperty("Version")
    String version;

   @JsonProperty("CVE")
    CVE[] listCVE;


}