package io.secdojo.ssc.parser.bdba.parser;

import java.io.IOException;
import java.util.Date;

import com.fortify.plugin.api.ScanBuilder;
import com.fortify.plugin.api.ScanData;
import com.fortify.plugin.api.ScanParsingException;


public class ScanParser {
    private final ScanBuilder scanBuilder;
    
	public ScanParser(final ScanData scanData, final ScanBuilder scanBuilder) {
		// Input doesn't contain any useful scan data, so we don't use this
		// this.scanData = scanData; 
		this.scanBuilder = scanBuilder;
	}
	
	public final void parse() throws ScanParsingException, IOException {
		// Required but not available in input
		scanBuilder.setEngineVersion("BDBA");
		scanBuilder.setScanDate(new Date()); 
		scanBuilder.completeScan();
	}
}