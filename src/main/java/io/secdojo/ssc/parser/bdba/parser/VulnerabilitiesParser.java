package io.secdojo.ssc.parser.bdba.parser;

import java.io.*;

import com.fortify.plugin.api.BasicVulnerabilityBuilder.Priority;
import com.fortify.plugin.api.ScanData;
import com.fortify.plugin.api.StaticVulnerabilityBuilder;
import com.fortify.plugin.api.VulnerabilityHandler;
import io.secdojo.ssc.parser.bdba.CustomVulnAttribute;
import io.secdojo.ssc.parser.bdba.domain.Component;
import io.secdojo.ssc.parser.bdba.domain.Object;
import com.fortify.util.ssc.parser.EngineTypeHelper;
import com.fortify.util.ssc.parser.json.ScanDataStreamingJsonParser;
import io.secdojo.ssc.parser.bdba.domain.CVE;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VulnerabilitiesParser {
    private static final Logger LOG = LoggerFactory.getLogger(VulnerabilitiesParser.class);

    private static final String ENGINE_TYPE = EngineTypeHelper.getEngineType();

    private final ScanData scanData;
    private final VulnerabilityHandler vulnerabilityHandler;


    public VulnerabilitiesParser(final ScanData scanData, final VulnerabilityHandler vulnerabilityHandler) {
        this.scanData = scanData;
        this.vulnerabilityHandler = vulnerabilityHandler;
    }


    /**
     * Main method to commence parsing the input provided by the configured {@link ScanData}.
     *
     */
    public final void parse() throws IOException {
        LOG.info("VulnerabilitiesParser parse starting");
        new ScanDataStreamingJsonParser()
                .handler("/components/*", Component.class, this::checkVulnerability)
                .parse(scanData);
    }

    public void checkVulnerability(Component vuln){
        if(vuln.getComponent() != null && !vuln.getComponent().equals("")){
            for (CVE cve : vuln.getListCVE()) {
                buildVulnerability(vuln, cve);
            }
        }
    }
    /**
     * Build the vulnerability from the given {@link Component}, using the configured {@link VulnerabilityHandler}.
     *
     */
    private void buildVulnerability(Component vuln, CVE cve) {
        LOG.info("VulnerabilitiesParser buildVulnerability");
        StaticVulnerabilityBuilder vb = vulnerabilityHandler.startStaticVulnerability(getInstanceId(vuln,cve));

        // Set meta-data
        vb.setEngineType(ENGINE_TYPE);
        vb.setAnalyzer("BDBA");

        // Set mandatory values to JavaDoc-recommended values
        vb.setAccuracy(5.0f);
        vb.setConfidence(2.5f);
        vb.setLikelihood(2.5f);


        // Set standard vulnerability fields based on input
        vb.setFileName(vuln.getComponent());
        vb.setPriority(getPriority(cve));
        vb.setCategory("Insecure Deployment");
        vb.setSubCategory("Unpatched Application");

        // Set custom attributes based on input
        vb.setStringCustomAttributeValue(CustomVulnAttribute.name,vuln.getComponent());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.version,vuln.getVersion());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.latestVersion,vuln.getLatestVersion());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.license,vuln.getLicense());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.cve, cve.getCVE());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.cvePublicationDate, cve.getCVEpublicationDate());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.cvss, cve.getCVSS());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.cvss3, cve.getCVSS3());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.cvssV3Vector, cve.getCVSS3vector());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.cvssV2Vector, cve.getCVSS2vector());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.vulnerabilityUrl, getVulnerabilityLink(cve.getVulnerabilityURL()));
        vb.setStringCustomAttributeValue(CustomVulnAttribute.object, getListObjects(cve));
        vb.setStringCustomAttributeValue(CustomVulnAttribute.noteType, cve.getNoteType());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.noteReason, cve.getNoteReason());
        vb.setStringCustomAttributeValue(CustomVulnAttribute.distributionPackage, vuln.getDistributionPackage());

        vb.completeVulnerability();
    }

    private Priority getPriority(CVE cve) {
        float score = Float.parseFloat(cve.getCVSS3());
        if(score < 4.0 ){
            return Priority.Low;
        }else if(score >= 4 && score < 7){
            return  Priority.Medium;
        }else if(score >= 7 && score < 9){
            return  Priority.High;
        }else {
            return  Priority.Critical;
        }
    }

    private  String getInstanceId(Component vuln, CVE cve) {
       return DigestUtils.sha256Hex(vuln.getComponent() + cve.getCVE() + getListObjectsName(cve));
    }

    private String getListObjectsName(CVE cve){
        StringBuilder sb = new StringBuilder();
        for (Object object : cve.getObjects()) {
            sb.append(object.getObject()).append(" ");
        }
        return sb.toString();
    }

    private String getListObjects(CVE cve){
        StringBuilder sb = new StringBuilder();
        for (Object object : cve.getObjects()) {
            sb.append("<b>Object: </b>").append(object.getObject()).append("</br>");
            sb.append("Full Path: ").append(object.getObjectFullPath()).append("</br></br>");
        }
        return sb.toString();
    }

    private String getVulnerabilityLink(String vuln){
        StringBuilder sb = new StringBuilder();
        sb.append("<a href=\"").append(vuln).append("\">").append(vuln).append("</a>");
        return sb.toString();
    }
}