package io.secdojo.ssc.parser.bdba;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum CustomVulnAttribute implements com.fortify.plugin.spi.VulnerabilityAttribute {

    // Custom attributes must have their types defined:
    name(AttrType.STRING),
    version(AttrType.STRING),
    latestVersion(AttrType.STRING),
    cve(AttrType.STRING),
    cvss(AttrType.STRING),
    cvePublicationDate(AttrType.STRING),
    object(AttrType.LONG_STRING),
    cvss3(AttrType.STRING),
    cvssV2Vector(AttrType.STRING),
    cvssV3Vector(AttrType.STRING),
    noteType(AttrType.STRING),
    noteReason(AttrType.STRING),
    vulnerabilityUrl(AttrType.STRING),
    license(AttrType.STRING),
    distributionPackage(AttrType.STRING),
    ;

	private final AttrType attributeType;

    CustomVulnAttribute(final AttrType attributeType) {
        this.attributeType = attributeType;
    }

    @Override
    public String attributeName() {
        return name();
    }

    @Override
    public AttrType attributeType() {
        return attributeType;
    }
}
