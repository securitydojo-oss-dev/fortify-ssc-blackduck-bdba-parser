package io.secdojo.ssc.parser.bdba.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class CVE {

    @JsonProperty("CVE")
    String CVE;
    @JsonProperty("CVE publication date")
    String CVEpublicationDate;
    @JsonProperty("CVSS")
    String CVSS;
    @JsonProperty("CVSS (Distribution)")
    String CVSSdistribution;
    @JsonProperty("CVSS vector (v2)")
    String CVSS2vector;
    @JsonProperty("CVSS vector (v3)")
    String CVSS3vector;
    @JsonProperty("CVSS3")
    String CVSS3;
    @JsonProperty("CVSS3 (Distribution)")
    String CVSS3distribution;
    @JsonProperty("Matching type")
    String matchingType;
    @JsonProperty("Vulnerability URL")
    String vulnerabilityURL;
    @JsonProperty("Note type")
    String noteType;
    @JsonProperty("Note reason")
    String noteReason;


    @JsonProperty("Objects")
    Object[] objects;
}
