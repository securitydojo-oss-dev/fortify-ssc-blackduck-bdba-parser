package io.secdojo.ssc.parser.bdba.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Object {

    @JsonProperty("Object")
    String object;
    @JsonProperty("Object SHA1")
    String objectSHA1;
    @JsonProperty("Object compilation date")
    String objectCompilationDate;
    @JsonProperty("Object full path")
    String objectFullPath;
}
